![Build Status](https://gitlab.com/rgaiacs/blogdown-gitlab/badges/master/build.svg)

---
# Website for model4you

This website is a [blogdown] website on [GitLab Pages](https://pages.gitlab.io).


---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  

- [The package](#the-package)
- [The website](#the-website)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## The package

The package is available on [CRAN](https://cran.r-project.org/package=model4you) 
and is developed on [R-Forge](https://r-forge.r-project.org/projects/partykit/).

## The website

Is in development and will soon show tutorials and additional information.
You can find it here: https://model4you.gitlab.io



