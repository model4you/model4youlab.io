---
title: "About"
date: "2018-02-14"
---

This website was created by Heidi Seibold on [GitLab](https://gitlab.com/model4you/model4you.gitlab.io/issues)
based on the template from [Raniere Silva](https://gitlab.com/rgaiacs/blogdown-gitlab). If you find errors please 
[let me know](https://gitlab.com/model4you/model4you.gitlab.io/issues).



<!-- 
This is a "hello world" example website for the [**blogdown**](https://github.com/rstudio/blogdown) package. The theme was forked from [@jrutheiser/hugo-lithium-theme](https://github.com/jrutheiser/hugo-lithium-theme) and modified by [Yihui Xie](https://github.com/yihui/hugo-lithium-theme).
-->
